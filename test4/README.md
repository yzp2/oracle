
班级：20软工2班
学号：202010513228
姓名：袁芷萍
# 实验4：PL/SQL语言打印杨辉三角

# 实验目的
掌握Oracle PL/SQL语言以及存储过程的编写。

# 实验内容

认真阅读并运行下面的杨辉三角源代码。

# 杨辉三角源代码
在终端运行以下代码可以实现：
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
### 杨辉三角打印完成
1
1	 1
1	 2	  1
1	 3	  3	   1
1	 4	  6	   4	    1
1	 5	  10	   10	    5	     1
1	 6	  15	   20	    15	     6	      1
1	 7	  21	   35	    35	     21       7        1
1	 8	  28	   56	    70	     56       28       8	1

PL/SQL 过程已成功完成。
![1](打印成功.png)

将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
运行这个存储过程即可以打印出N行杨辉三角。
写出创建YHTriangle的SQL语句。
以下是创建过程的代码以及运行过程打印出杨辉三角：
create or replace PROCEDURE YHTRIANGLE AS 
BEGIN
 declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
    END;

END YHTRIANGLE;
![2](创建过程.png)
## 实验完成

# 实验结果与分析
可以在终端运行杨辉三角的代码打印，创建存储过程，然后运行存储过程就能实现杨辉三角的打印，这个过程是很方便的，可以将代码存储起来需要使用的时候再使用，给我们提供了极大的便利。
[1]:base64