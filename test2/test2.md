# 实验二：用户及权限管理

## 实验目的
  - 掌握用户管理、角色管理，权限维护与分配能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容
- Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：
    - 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
    - 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
    - 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据

## 实验过程
  1.创建本地角色con_res_yzp和用户yzp_sale,将connect、resource和CREATE VIEW三种权限授权给con_res_yzp,授权和分配空间。
```mysql
SQL> CREATE ROLE con_res_yzp;
SQL> CREATE ROLE con_res_yzp;
SQL> GRANT connect,resource,CREATE VIEW TO con_res_yzp;
SQL> CREATE USER yzp_sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
SQL> ALTER USER sale default TABLESPACE "USERS";
SQL> ALTER USER yzp_sale default TABLESPACE "USERS";
SQL> ALTER USER yzp_sale QUOTA 50M ON users;
SQL> GRANT con_res_yzp TO yzp_sale;
```
  实验结果
  ![1](1.png)
  可以看到成功创建了角色con_res_yzp和用户yzp_sale,同时授权成功。

 2.新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```mysql
 SQL> show user;
 USER 为 "YZP_SALE"
 SQL> SELECT * FROM session_privs;
 SQL> SELECT*FROM session_roles;
 SQL> CREATE TABLE CUSTOMERS(id number,name varchar(50)) TABLESPACE "USERS";
 表已创建。
 SQL> INSERT INTO customers(id,name)VALUES(1,'li');
 SQL> INSERT INTO customers(id,name)VALUES(2,'sun');
 SQL> INSERT INTO customers(id,name)VALUES(3,'wu');
 SQL> CREATE VIEW customers_view AS SELECT name FROM customers;
 视图已创建。
 SQL> GRANT SELECT ON customers_view TO hr;
 SQL> SELECT * FROM customers_view;
```
  实验结果：
  - 成功创建CUSTOMERS表和视图customers_view,
![2](2.png)
  - 并向表中插入三条数据
![3](3.png)
 3.用户hr连接到pdborcl，查询sale授予它的视图customers_view

```mysql
SQL> SELECT *FROM yzp_sale.customers;
SELECT *FROM yzp_sale.customers
                      *
第 1 行出现错误:
ORA-00942: 表或视图不存在
SQL> SELECT *FROM yzp_sale.customers_view;
```
实验结果：因为创建了视图customer_view,所以查询时不能直接查询表customers而要查询customers_view。
![4](4.png)
## 概要文件设置，用户最多登录时最多只能错误3次
```mysql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```
实验结果：设置后，输入3次错误密码后，用户就会被锁定。当锁定后通过system用户登录，输入alter user sale unlock命令解锁。
![5](5.png)

## 查看数据库的使用情况

```mysql
SQL> SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';
SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
     where  a.tablespace_name = b.tablespace_name;
```
实验结果：新用户sale使用默认表空间users存储表的数据，随着用户往表中插入数据，表空间的磁盘使用量会增加。
![6](6.png)

## 删除用户和角色
  ```mysql
  SQL> drop role con_res_yzp;
  SQL> drop user yzp_sale cascade;

  ```
  实验结果：
  ![7](7.png)

[1]:base64
