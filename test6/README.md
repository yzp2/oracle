期末设计：基于Oracle数据库的商品销售系统的设计

学号：202010513228
姓名：袁芷萍
班级：20软工2班


实验目的

使用oracle完成商品销售的一系列的数据库文件的编写，构建需要的数据表，完成实验设计

实验内容

1.数据库表结构
数据库表索引
表名	中文名
Item	商品表
Rack	货架
Provide	供货商表
StockIn	入库表
StockOut	出库表
User	用户表
StockHold	库存表
Sale	销售记录
Buy	顾客消费表
Stock	仓库表
Customer	顾客表


用户表（User）
字段名	字段类型	长度	主/外键	字段约束	对应中文名
UserID	verchar	30	P	NOT NULL	用户账号
UserPW	verchar	50		NOT NULL	用户密码
UserName	verchar	30		NOT NULL	用户名字
UserType	verchar	20		NOT NULL	用户类型

库存表(StockHold)
字段名	字段类型	长度	主/外键	字段约束	对应中文名
ItemID	verchar	30		NOT NULL	商品号
StockID	verchar	50		NOT NULL	仓库编号
StockCount	number	6		NOT NULL	库存量
MinCount	date	4		NOT NULL	最低库存量


销售记录（Sale）
字段名	字段类型	长度	主/外键	字段约束	对应中文名
ItemID	verchar	6		NOT NULL	商品号
ItemName	verchar	50		NOT NUL	商品名
SaleNum	number	4		NOT NULL	数量
SaleDate	date	8		NOT NULL	销售日期
ItemPrice	money	50		NOT NULL	销售价格

顾客消费表(Buy)
字段名	字段类型	长度	主/外键	字段约束	对应中文名
ReceiptID	number	6		NOT NULL	单据号
ItemID	varchar	6		NOT ULL	商品号
ItemName	verchar	50		NOT NULL	商品名
ItemPrice	money	50		NOT NULL	单价
SaleNum	number	4		NOT NULL	数量
Sum	money			NOT NULL	金额合计
BuyDate	date			NOT NULL	日期

仓库表(Stock)
字段名	字段类型	长度	主/外键	字段约束	对应中文名
StockID	verchar	4	P	NOT NULL	仓库编号
StockAdd	verchar	50			仓库地址
StockVol	verchar	50			仓库容量

顾客表（Customer）
字段名	字段类型	长度	主/外键	字段约束	对应中文名
ReceiptID	number	6	P	NOT NULL	单据号


2.数据库实现


 一，创建基本表
/*==============================================================*/
/* Table: Buy                                                   */
/*==============================================================*/
CREATE TABLE Buy (
   ReceiptID            NUMBER(6)                      DEFAULT 1 NOT NULL,
   ItemID               VARCHAR2(6)                     NOT NULL,
   ItemName             VARCHAR2(50)                    NOT NULL,
   ItemPrice            NUMBER(30)                      NOT NULL,
   SaleNum              NUMBER(4)                       NOT NULL,
   Sum                  NUMBER(8,2)                     NOT NULL,
   BuyDate              DATE                            NOT NULL
)
/
/*==============================================================*/
/* Table: Customer                                              */
/*==============================================================*/
CREATE TABLE Customer  (
   ReceiptID            NUMBER(6)                      default 1 not null,
   constraint PK_CUSTOMER primary key (ReceiptID)
)
/

/*==============================================================*/
/* Table: Item                                                  */
/*==============================================================*/
CREATE TABLE Item  (
   ItemID               VARCHAR2(6)                     not null,
   ItemName             VARCHAR2(50)                    not null,
   ProduceDate          DATE                            not null,
   EndDate              NUMBER                          not null,
   constraint PK_ITEM primary key (ItemID)
)
/

/*==============================================================*/
/* Table: Provide                                               */
/*==============================================================*/
CREATE TABLE Provide  (
   ProvideID            VARCHAR2(10)                    not null,
   ProvideName          VARCHAR2(50)                    not null,
   ProvideAddress       VARCHAR2(250),
   ProvidePhone         VARCHAR2(25)                    not null,
   constraint PK_PROVIDE primary key (ProvideID)
)
/

/*==============================================================*/
/* Table: Rack                                                  */
/*==============================================================*/
CREATE TABLE Rack  (
   RackID               VARCHAR2(4)                     not null,
   RackAdd              VARCHAR2(6)                     not null,
   RackVol              NUMBER                          not null,
   constraint PK_RACK primary key (RackID)
)
/

/*==============================================================*/
/* Table: Sale                                                  */
/*==============================================================*/
CREATE TABLE Sale  (
   ItemID               VARCHAR2(6)                     not null,
   ItemName             VARCHAR2(50)                    not null,
   SaleNum              VARCHAR2(50)                    not null,
   SaleDate             DATE                            not null,
   ItemPrice            NUMBER(30),
   constraint PK_SALE primary key (ItemID)
)
/

/*==============================================================*/
/* Index: Index_SaleDate                                        */
/*==============================================================*/
CREATE INDEX Index_SaleDate ON Sale(SaleDate ASC);)
/

/*==============================================================*/
/* Table: Stock                                                 */
/*==============================================================*/
CREATE TABLE Stock  (
   StockID              VARCHAR2(4)                     not null,
   StockAdd             VARCHAR2(50)                    not null,
   StockVol             VARCHAR2(50)                    not null,
   constraint PK_STOCK primary key (StockID)
)
/

/*==============================================================*/
/* Table: StockHold                                             */
/*==============================================================*/
CREATE TABLE StockHold  (
   ItemID               VARCHAR2(6)                     not null,
   StockID              VARCHAR2(4)                     not null,
   StockCount           NUMBER(6)                       not null
      constraint CKC_STOCKCOUNT_STOCKHOL check (StockCount >= 0),
   MinCount             NUMBER(4),
   constraint PK_STOCKHOLD primary key (ItemID, StockID)
)
/

/*==============================================================*/
/* Index: Relationship_4_FK                                     */
/*==============================================================*/
CREATE INDEX Relationship_4_FK on StockHold (
   StockID ASC
)
/

/*==============================================================*/
/* Index: Relationship_5_FK                                     */
/*==============================================================*/
CREATE INDEX Relationship_5_FK ON StockHold (
   ItemID ASC
);
/

/*==============================================================*/
/* Table: StockIn                                               */
/*==============================================================*/
CREATE TABLE StockIn  (
   ItemID               VARCHAR2(6)                     not null,
   ItemName             VARCHAR2(50),
   ProvideID            VARCHAR2(10)                    not null,
   ItemPriceIn          NUMBER(30)                      not null,
   InCount              NUMBER(6),
   TotalPrice           NUMBER(8,2),
   InDate               DATE,
   ProduceDate          DATE                            not null,
   EndDate              NUMBER
)
/

/*==============================================================*/
/* Index: Relationship_2_FK                                     */
/*==============================================================*/
CREATE INDEX Relationship_2_FK on StockIn (
   ItemID ASC
)
/

/*==============================================================*/
/* Index: Relationship_3_FK                                     */
/*==============================================================*/
CREATE INDEX Relationship_3_FK on StockIn (
   ProvideID ASC
)
/

/*==============================================================*/
/* Index: Index_InDate                                          */
/*==============================================================*/
create index Index_InDate on StockIn (
   InDate ASC
)
/

/*==============================================================*/
/* Table: StockOut                                              */
/*==============================================================*/
CREATE TABLE StockOut  (
   ItemID               VARCHAR2(6)                     not null,
   RackID               VARCHAR2(4)                     not null,
   ItemName             VARCHAR2(50)                    not null,
   OutCount             NUMBER(6)                       not null,
   OutDate              DATE                            not null,
   ItemPrice            NUMBER(30)                      not null,
   Rebate               FLOAT(3)
)
/

/*==============================================================*/
/* Index: Relationship_6_FK                                     */
/*==============================================================*/
CREATE INDEX Relationship_6_FK on StockOut (
   RackID ASC
)
/

/*==============================================================*/
/* Index: Relationship_7_FK                                     */
/*==============================================================*/
CREATE INDEX Relationship_7_FK on StockOut (
   ItemID ASC
)
/

/*==============================================================*/
/* Index: Index_OutDate                                         */
/*==============================================================*/
create index Index_OutDate on StockOut (
   OutDate ASC
)
/

/*==============================================================*/
/* Table: "User"                                                */
/*==============================================================*/
CREATC TABLE "User"  (
   UserID               VARCHAR2(30)                    not null,
   UserPW               VARCHAR2(50)                    not null,
   UserName             VARCHAR2(30)                    not null,
   UserType             VARCHAR2(20)                    not null,
   constraint PK_USER primary key (UserID)
)
/
/*----------创建视图----------*/

/*==============================================================*/
/* View: v_Item                                                 */
/*==============================================================*/
CREATE OR REPLACE VIEW v_Item AS
SELECT
   Item.ItemID,
   Item.ItemName AS Item_ItemName,
   StockHold.StockCount,
   StockOut.ItemPrice
FROM
   Item
   JOIN StockHold ON Item.ItemID = StockHold.ItemId
   JOIN StockOut ON Item.ItemID = StockOut.ItemID
WITH READ ONLY;/

/*==============================================================*/
/* View: v_Provide                                              */
/*==============================================================*/
CREATE OR REPLACE VIEW v_Provide AS
SELECT
   Provide.ProvideName,
   StockIn.ItemName,
   StockIn.ItemPriceIn
FROM
   Provide
   JOIN StockIn ON Provide.ProvideID=StockIn.ProvideID
WITH READ ONLY;/
/*----------创建存储过程----------*/

CREATE OR REPLACE PROCEDURE P_Buy
(
p_ItemID IN VARCHAR2,
p_SaleNum IN NUMBER,
p_ItemPrice NUMBER,
p_BuyDate IN DATE DEFAULT SYSDATE
)
AS
p_Sum NUMBER;
p_ReceiptID NUMBER;
p_ItemName VARCHAR2(6);
BEGIN
SELECT * INTO p_ReceiptID FROM Customer;
SELECT ItemName INTO p_ItemName FROM Item WHERE Item.ItemID=p_ItemID;
p_Sum:=p_ItemPrice*p_SaleNum;
INSERT INTO Buy(ReceiptID,ItemID,ItemName,ItemPrice,SaleNum,Sum,BuyDate)
VALUES(p_ReceiptID,p_ItemID,p_ItemName,p_ItemPrice,p_SaleNum,p_Sum,p_BuyDate);
END;
/


CREATE OR REPLACE PROCEDURE P_StockIn (
    p_ItemID IN VARCHAR2,
    p_ProvideID IN VARCHAR2,
    p_ItemPriceIn IN NUMBER,
    p_InCount IN NUMBER,
    p_EndDate IN NUMBER,
    p_ProduceDate IN DATE,
    p_InDate IN DATE DEFAULT SYSDATE
) AS
    p_TotalPrice NUMBER;
    p_ItemName VARCHAR2(50);
BEGIN
    p_TotalPrice:=p_ItemPriceIn*p_Incount;
    SELECT ItemName INTO p_ItemName FROM Item WHERE Item.ItemID=p_ItemID;
    INSERT INTO StockIn(ItemID,ItemName,ProvideID,ItemPriceIn,InCount,TotalPrice,EndDate,ProduceDate,InDate)
    VALUES(p_ItemID,p_ItemName,p_ProvideID,p_ItemPriceIn,p_InCount,p_TotalPrice,p_EndDate,p_ProduceDate,p_InDate);
END;
/

CREATE OR REPLACE PROCEDURE P_StockOut (
    p_ItemID IN VARCHAR2,
    p_RackID IN VARCHAR2,
    p_OutCount IN NUMBER,
    p_ItemPrice IN NUMBER,
    p_Rebate IN FLOAT,
    p_OutDate IN DATE DEFAULT SYSDATE
) AS
    count1 NUMBER;
    p_ItemName VARCHAR2(50);
BEGIN
    SELECT StockCount INTO count1 FROM StockHold WHERE StockHold.ItemID=p_ItemID;
    SELECT ItemName INTO p_ItemName FROM Item WHERE Item.ItemID=p_ItemID;
    IF count1>p_OutCount THEN
        INSERT INTO StockOut(ItemID,RackID,ItemName,OutCount,ItemPrice,Rebate,OutDate)
        VALUES(p_ItemID,p_RackID,p_ItemName,p_OutCount,p_ItemPrice,p_Rebate,p_OutDate);
    ELSE 
        dbms_output.put_line('仓库存储量不足'); 
    END IF;
END;
/

二，创建触发器

CREATE OR REPLACE TRIGGER Buy_Sale 
AFTER INSERT ON Sales
 FOR EACH ROW 
BEGIN 
IF :NEW.Type = 'Buy' THEN 
UPDATE Products 
SET Quantity = Quantity + :NEW.Quantity 
WHERE ProductID = :NEW.ProductID; 
ELSIF :NEW.Type = 'Sale' THEN 
UPDATE Products 
SET Quantity = Quantity - :NEW.Quantity
   WHERE ProductID = :NEW.ProductID;
  END IF;
 END;

CREATE OR REPLACE TRIGGER ItemAdd
AFTER INSERT ON Item
FOR EACH ROW
BEGIN
  IF INSERTING THEN
    INSERT INTO Stockhold(ItemID, StockID, StockCount, MinCount)
    VALUES(:new.ItemID, '1', 0, 20);
    INSERT INTO Sale(ItemID, ItemName, SaleNum, SaleDate, ItemPrice)
    VALUES(:new.ItemID, :new.ItemName, 0, SYSDATE, 0);
  END IF;
END;/


CREATE OR REPLACE TRIGGER StockHold_notice
AFTER UPDATE OF ItemID, StockID ON StockHold
FOR EACH ROW
BEGIN
    IF :new.StockCount < :new.MinCount THEN
        dbms_output.put_line('库存较低，请及时进货');
    END IF;
END;
/

CREATE OR REPLACE TRIGGER Stocki
AFTER INSERT ON StockIn
FOR EACH ROW
BEGIN
    IF inserting THEN
        UPDATE StockHold SET StockCount = StockCount + :new.InCount WHERE StockHold.ItemID = :new.ItemID;
    END IF;
END;
/

CREATE OR REPLACE TRIGGER Stocko
AFTER INSERT ON StockOut
FOR EACH ROW
BEGIN
    IF inserting THEN
        UPDATE StockHold SET Stockcount = StockCount - :new.OutCount WHERE StockHold.ItemID = :new.ItemID;
    END IF;
END;
/

结论
 通过期末设计对oracle有了更深层次的认识，虽然相较于MySQL，oracle会更繁琐一些，但是oracle具有高度可扩展性，可以轻松地扩展到大型企业级应用程序。以及高度安全性，Oracle数据库提供了高级的安全功能，包括访问控制、加密和审计等。提供了灵活的数据模型和数据访问方式，可以满足各种不同的应用需求。对Oracle还是会继续运用以及学习。
